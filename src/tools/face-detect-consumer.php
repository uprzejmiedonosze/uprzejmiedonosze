<?PHP namespace queue;

require_once(__DIR__ . '/../../vendor/autoload.php');
require_once(__DIR__ . '/../inc/include.php');
require_once(__DIR__ . '/../inc/integrations/curl.php');

logger("Starting face-blur-consumer...", true);

$consumer = function (string $appId): void {
  try {
    $app = \app\get($appId);
    if (isset($app->faces->count)) {
      logger("Faces already detected in $appId");
      return;
    }

    $filename = ROOT . $app->contextImage->url;
    $url = "http://localhost:2000/detect/$filename";
    $faces = \curl\request($url, [], "FaceRecogniton");

    try {
      \semaphore\acquire($appId, "face-detect-consumer");
      $app = \app\get($appId);
      $app->faces = $faces;
      \app\save($app);
    } finally {
      \semaphore\release($appId, "face-detect-consumer");
    }

    logger("Detected faces in $appId: " . ($faces->count ?? 0)); 
    sleep(5);
  } catch (\Exception $e) {
    $plateId = $app->carInfo->plateId ?? '[plateId]';
    logger("ERROR: Failed detect face in $appId ($plateId) " . $e->getMessage(), true);
    sleep(30);
  }
};

consume($consumer);
