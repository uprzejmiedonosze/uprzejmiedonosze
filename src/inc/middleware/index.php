<?PHP

require(__DIR__ . '/AuthMiddleware.php');
require(__DIR__ . '/CsvMiddleware.php');
require(__DIR__ . '/HtmlMiddleware.php');
require(__DIR__ . '/XlsMiddleware.php');
require(__DIR__ . '/JsonMiddleware.php');
require(__DIR__ . '/PdfMiddleware.php');
require(__DIR__ . '/SessionMiddleware.php');
require(__DIR__ . '/HtmlErrorRenderer.php');
require(__DIR__ . '/JsonBodyParser.php');
require(__DIR__ . '/JsonErrorRenderer.php');
require(__DIR__ . '/JpegMiddleware.php');
