<?PHP

require(__DIR__ . '/Store.php');
require(__DIR__ . '/Cache.php');
require(__DIR__ . '/UserStore.php');
require(__DIR__ . '/AppStore.php');
require(__DIR__ . '/GlobalStats.php');
require(__DIR__ . '/RecydywaStore.php');
require(__DIR__ . '/Webhook.php');
require(__DIR__ . '/Semaphore.php');
require(__DIR__ . '/Queue.php');
require(__DIR__ . '/Patronite.php');