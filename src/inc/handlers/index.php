<?PHP

require(__DIR__ . '/ApplicationHandler.php');
require(__DIR__ . '/SessionApiHandler.php');
require(__DIR__ . '/UserHandler.php');
require(__DIR__ . '/StaticPagesHandler.php');
require(__DIR__ . '/CsvHandler.php');
require(__DIR__ . '/XlsHandler.php');
require(__DIR__ . '/ErrorHandler.php');
require(__DIR__ . '/JpegHandler.php');
require(__DIR__ . '/WebhooksHandler.php');
