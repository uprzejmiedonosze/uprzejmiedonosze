user  www-data www-data;
worker_processes  auto;

pid        /var/run/nginx.pid;

events {
    worker_connections  1024;
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '"$request" $status "$http_referer" "$http_x_forwarded_for" $request_time';

    sendfile        on;

    keepalive_timeout  65;

    # Gzip Settings
    gzip on;
    gzip_disable "msie6";
    gzip_comp_level 6;
    gzip_http_version 1.1;
    gzip_min_length 1000;
    gzip_vary on;
    gzip_types text/plain text/css application/x-javascript text/xml application/xml application/xml+rss text/javascript image/x-icon image/bmp image/svg+xml application/javascript;

    upstream php {
        server unix:/run/php/php8.2-fpm.sock;
    }

    proxy_buffering on;

    server {
        listen 80;

        etag on;
        
        error_log   /var/log/uprzejmiedonosze.net/error.log debug;
        access_log  /var/log/uprzejmiedonosze.net/access.log main;
        error_page 404 /404.html;
     
        root /var/www/uprzejmiedonosze.localhost/webapp/public;    
       
        location ~* ^/(cdn|cdn2|\.(js|css|png|jpg|jpeg|gif|ico))$ {
            access_log off;
            expires max;
            try_files $uri =404;
        }

        location / {
            try_files $uri /index.php$is_args$args;
        }
        
        location ~ \.php$ {
            try_files $uri =404;
            include nginx_php_configuration.conf;
        }

        location ~* /img-.*\.php$ {
            try_files $uri /index.php$is_args$args;
            expires 30d;
            add_header Pragma public;
            add_header Cache-Control "public";
            log_not_found off;
        }

        set $CDN_ROOT "/var/www/uprzejmiedonosze.localhost";
        location ^~ /cdn/ {
            try_files $uri =404;
            root $CDN_ROOT;
        }
        location ^~ /cdn2/ {
            try_files $uri =404;
            root $CDN_ROOT;
        }
        location ~ ^/api/rest {
            try_files $uri $uri/ /api/rest/index.php$is_args$args;
                location ~ \.php$ {
                    include nginx_php_configuration.conf;
                }
            }
    }
}